package com.echosense.extentreport;

import java.io.File;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
public class SetupExtentReport {

	public static ExtentHtmlReporter extent;
	public static ExtentTest logger;

	@BeforeSuite
	public void setUp() {
		extent = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/ExtentReport.html");
	//	extent.addSystemInfo("Host Name", "Localhost").addSystemInfo("Environment", "Ubuntu 16.0LTS")
	//			.addSystemInfo("User Name", "Shalini Siwal");
		// loading the external xml file (i.e., extent-config.xml)
	//	extent.loadConfig(new File(System.getProperty("user.dir") + "/extent-config.xml"));
	}

	/**
	 * 
	 * @param result
	 */
	@AfterMethod
	public void getResult(ITestResult result) {
		if (result.getStatus() == ITestResult.FAILURE) {
//			logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getName());
//			logger.log(LogStatus.FAIL, "Test Case Failed is " + result.getThrowable());
		} else if (result.getStatus() == ITestResult.SUCCESS) {
	//		logger.log(LogStatus.PASS, "Test Case Passed is " + result.getName());
		} else {
		//	logger.log(LogStatus.SKIP, "Test Case Skipped is " + result.getName());
		}
	}

	@AfterSuite
	public void tearDown() {
//		extent.flush();
	}

}
