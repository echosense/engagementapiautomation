package com.echosense.email;


import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * 
 * @author shalini
 *
 */
public class SendEmail {
    public void sendEmailReport() {

        // Create object of Property file
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        Session session = Session.getInstance(props, new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("echosense123@gmail.com", "echosense@123");// Specify the Username and the
                                                                                    // Password
            }
        });
        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("echosense123@gmail.com"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse("shalini@echosense.in"));
            message.setSubject("Report : Website Sanity Testing");
            BodyPart messageBodyPart1 = new MimeBodyPart();
            messageBodyPart1.setText("\n\n\n\n Please find the report attached in this email. \n\n\n\n");
            MimeBodyPart messageBodyPart2 = new MimeBodyPart();
            String filename = "finalReport.html";
            String filePath = "./ExtentReport/";
            DataSource source = new FileDataSource(filePath + filename);
            messageBodyPart2.setDataHandler(new DataHandler(source));
            messageBodyPart2.setFileName(filename);
            Multipart multipart = new MimeMultipart();
            // add body part 1
            multipart.addBodyPart(messageBodyPart1);
            // add body part 2
            multipart.addBodyPart(messageBodyPart2);
            // set the content
            message.setContent(multipart);
            // finally send the email
            Transport.send(message);
        } catch (MessagingException e) {

            throw new RuntimeException(e);

        }
    }

}
