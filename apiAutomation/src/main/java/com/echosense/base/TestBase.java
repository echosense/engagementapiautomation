package com.echosense.base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.echosense.email.SendEmail;

public class TestBase {

    public static Properties prop;
    static ExtentHtmlReporter htmlReporter;
    protected static ExtentReports extent;
    static ExtentTest extentLogger;
    
    SendEmail sendEmail = new SendEmail();
    static final Logger log = LogManager.getLogger(TestBase.class);

    /**
     * @author shalini This constructor is use to load config file
     */
    public TestBase() {
        try {
            prop = new Properties();
            FileInputStream ip = new FileInputStream(
                    System.getProperty("user.dir") + "/src/main/java/com/echosense/config/config.properties");
            prop.load(ip);
            log.debug("Reading property file.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        try {
            htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/ExtentReport/finalReport.html");
            extent = new ExtentReports();
            extent.attachReporter(htmlReporter);
            extent.setSystemInfo("Host Name", "Echosense");
            extent.setSystemInfo("Environment", "test");
            extent.setSystemInfo("User Name", "Shalini");

            htmlReporter.config().setDocumentTitle("Extent Report");
            htmlReporter.config().setReportName("Website Sanity Testing");
            htmlReporter.config().setTheme(Theme.DARK);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    /**
     * @author shalini Run whenever you need to send the final report
     */
    public void sendReport() {
        log.debug("Generating final report.");
        extent.flush();
        log.debug("Sending final report on email");
        sendEmail.sendEmailReport();
    }
}
