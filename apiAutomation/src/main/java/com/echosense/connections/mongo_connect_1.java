package com.echosense.connections;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

import org.bson.Document;

import com.mongodb.*;
import com.mongodb.MongoClient;
import com.mongodb.client.*;

public class mongo_connect_1 {

	public static void main(String args[]) {
		String user = "secureUser";
		String pwd = "bethechange";
		MongoClientURI provisiondb = new MongoClientURI(
				"mongodb://secureUser:bethechange@localhost:27017/?authSource=provisiondb&authMechanism=SCRAM-SHA-1");
		MongoClient mongoClient = new MongoClient(provisiondb);
		MongoDatabase db = mongoClient.getDatabase("provisiondb");
		///String database = db.toString();
		// Drop the collections first and import new collections
		String col[] = { "campaign", "appInformation", "partner" };
		int len = col.length;
		for (int i = 0; i < len; i++) {
			//System.out.print("*******" + database);
			MongoCollection<Document> dropCollection = db.getCollection(col[i]);
			dropCollection.drop();
			String Host = "localhost";
			String Port = "27017";
			String fileName = "/home/shalini/api-automation-workspace/apiAutomation/src/main/resources/" + col[i]
					+ ".csv";
			String command = "mongoimport --host " + Host + " --port " + Port + " -u " + user + " -p " + pwd + " --db "
					+ "provisiondb" + " --collection " + col[i] + " --type csv --file " + fileName + " --headerline";
			try {
				System.out.println(command);

				Process process = Runtime.getRuntime().exec(command);
				int waitFor = process.waitFor();
				System.out.println("waitFor:: " + waitFor);
				BufferedReader success = new BufferedReader(new InputStreamReader(process.getInputStream()));
				BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

				String s = "";
				while ((s = success.readLine()) != null) {
					System.out.println("\n !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+s);
				}

				String e = "";
				while ((e = error.readLine()) != null) {
					System.out.println("\n SSSSSSSSSSSSSSSSSSSSSSS Std ERROR : " + e);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
