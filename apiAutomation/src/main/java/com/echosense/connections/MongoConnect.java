package com.echosense.connections;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.echosense.base.TestBase;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;

/**
 * 
 * @author shalini
 *
 */
public class MongoConnect extends TestBase{
	protected String user = prop.getProperty("db_user");
	protected String pwd = prop.getProperty("db_password");
	protected MongoDatabase db;
	String db_name = "provisiondb";
	static final Logger log = LogManager.getLogger(MongoConnect.class);
	
	public MongoConnect() {
		try {
			//MongoClientURI provisiondb = new MongoClientURI(
			//		"mongodb://"+user+":"+pwd+"@"+prop.getProperty("db_dns")+":"+prop.getProperty("db_port")+"/?authSource="+db_name+"&authMechanism=SCRAM-SHA-1");
			//MongoClientURI provisiondb = new MongoClientUR(172.31.27.27, 27017);
			MongoClient mongoClient = new MongoClient("localhost", 27017);
			db = mongoClient.getDatabase(db_name);
		}catch (Exception e) {
			log.error(e);
		}
	}
	

}
