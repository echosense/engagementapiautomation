package com.echosense.readwritedata;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;



import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class ReadExcelData {
	//ExtentReports extent;
	//static ExtentTest logger;

	public static String[][] getExcelData(String fileName, String sheetName) throws BiffException {
        String[][] arrayExcelData = null;
        
        try {
        	//logger.log(LogStatus.INFO, "Read excel data method is excuting" );
            FileInputStream fs = new FileInputStream(fileName);
            Workbook wb = Workbook.getWorkbook(fs);
            Sheet sh = wb.getSheet(sheetName);

            int totalNoOfCols = sh.getColumns();
            int totalNoOfRows = sh.getRows();
            System.out.println("number of columns are : " + totalNoOfCols);
            System.out.println("number of rows are : " + totalNoOfRows);
            
            arrayExcelData = new String[totalNoOfRows-1][totalNoOfCols-1];
            
            for (int i= 1 ; i < totalNoOfRows; i++) {

                for (int j=1 ; j < totalNoOfCols; j++) {
                    arrayExcelData[i-1][j-1] = sh.getCell(j, i).getContents();
                }

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
           // logger.log(LogStatus.ERROR, "Read excel data method has been failed" );
        } catch (IOException e) {
            e.printStackTrace();
            e.printStackTrace();
            //logger.log(LogStatus.ERROR, "Read excel data method has been failed" );
        }
        return arrayExcelData;
    }

    }

        

