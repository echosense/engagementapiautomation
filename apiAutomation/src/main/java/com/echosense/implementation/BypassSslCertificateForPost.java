package com.echosense.implementation;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSession;

import org.apache.logging.log4j.*;

import com.aventstack.extentreports.ExtentTest;


/**
 * 
 * @author shalini
 *
 */
public class BypassSslCertificateForPost {
	static final Logger log = LogManager.getLogger(BypassSslCertificateForPost.class);
//	ExtentReports extent;
	GetApiResponse getApiResponse = new GetApiResponse();
	private final String USER_AGENT = "Mozilla/5.0";
	int statusCode;

	/**
	 * 
	 * @param apiUrl
	 * @param logger
	 * @param urlParameters
	 * @param logger 
	 */
	public Integer skipSslCertificateForPost(String apiUrl, String urlParameters, ExtentTest logger) {
		String https_url = apiUrl;
		URL url;
		try {
			url = new URL(https_url);
			HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
			// add reuqest header
			log.debug("Setting header proprties");
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Content-Type", "application/json; charset=utf8");
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

			con.setDoOutput(true);

			// Guard against "bad hostname" errors during handshake.
			con.setHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String host, SSLSession sess) {
					System.out.println("host name:" + host);
					if (host.equals("localhost")) {
						log.debug("Certificate has been skip");
						return true;

					} else {
						log.debug("Certificate has not been skip");
						return true;
					}
				}
			});

			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(urlParameters);
			wr.flush();
			wr.close();

			statusCode = getApiResponse.getApiResponse(con, logger);

			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			System.out.println(response.toString());
			log.debug("API response is: " + response.toString());

		} catch (MalformedURLException e) {
			//e.printStackTrace();
			log.debug("Certificate skip process has been failed");
		} catch (IOException e) {
			//e.printStackTrace();
			log.error("Certificate skip process has been failed");
		}
		return statusCode;
	}

}
