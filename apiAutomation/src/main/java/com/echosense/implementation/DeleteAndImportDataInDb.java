package com.echosense.implementation;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bson.Document;

import com.echosense.base.TestBase;
import com.echosense.connections.MongoConnect;
import com.mongodb.client.MongoCollection;

public class DeleteAndImportDataInDb extends MongoConnect {
	// String user;
	// String pwd;
	static final Logger log = LogManager.getLogger(DeleteAndImportDataInDb.class);

	public void deleteAndImportDataFromDb(String csvName) {
		// Drop the collections first and import new collections
		//String col[] = { "campaign", "appInformation", "partner" };
		//int len = col.length;
		//for (int i = 0; i < len; i++) {
			// System.out.print("*******" + database);
			MongoCollection<Document> dropCollection = super.db.getCollection(csvName);
			dropCollection.drop();
			////String Host = "localhost";
			//String Port = "27017";
			String fileName = System.getProperty("user.dir")+"/src/main/resources/" + csvName
					+ ".csv";
			String command = "mongoimport --host " + prop.getProperty("db_dns") + " --port " + prop.getProperty("db_port") + " --db " + "provisiondb" + " --collection " + csvName + " --type csv --file " + fileName
					+ " --headerline";
			try {
				System.out.println(command);

				Process process = Runtime.getRuntime().exec(command);
				int waitFor = process.waitFor();
				System.out.println("waitFor:: " + waitFor);
				BufferedReader success = new BufferedReader(new InputStreamReader(process.getInputStream()));
				BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));

				String s = "";
				while ((s = success.readLine()) != null) {
					System.out.println(s);
				}

				while ((s = error.readLine()) != null) {
					System.out.println("\n Std ERROR : " + s);
			
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

