package com.echosense.implementation;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.util.IOUtils;
import org.testng.Assert;

import com.aventstack.extentreports.ExtentTest;

/***
 * 
 * @author shalini
 *
 */

public class GetApiResponse {
	static final Logger log = LogManager.getLogger(GetApiResponse.class);
	/**
	 * 
	 * @param con
	 * @param logger
	 * @throws IOException
	 */
	public void getAndVerifyResponseCode(HttpsURLConnection con) throws IOException {
		int responseCode = con.getResponseCode();
		log.debug("API response code is: " + responseCode);
		Assert.assertEquals(responseCode, 200, "Incorrect status code " + responseCode + " return");
	}

	/**
	 * 
	 * @param con
	 * @param logger 
	 * @return
	 * @throws IOException
	 */
	public Integer getApiResponse(HttpsURLConnection con, ExtentTest logger) throws IOException {
		int responseCode = con.getResponseCode();
		if(responseCode==200) {
		InputStream is = con.getInputStream();
		Scanner sc = new Scanner(is);
		StringBuffer sb = new StringBuffer();
		while(sc.hasNext()) {
			sb.append(sc.nextLine());
		}
		logger.info("API response is : " +sb.toString());
		}else {
			logger.info("API response is : null as API status code is : " + responseCode);
		}
		log.debug("API response code is: " + responseCode);
		return responseCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getUnixTimeStamp() {
		long unixTime = System.currentTimeMillis() / 1000L;
		return Long.toString(unixTime);
	}

}
