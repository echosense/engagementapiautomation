package com.echosense.testcases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.echosense.base.TestBase;
import com.echosense.implementation.BypassSslCertificateForPost;
import com.echosense.implementation.DeleteAndImportDataInDb;
import com.echosense.readwritedata.ReadExcelData;

import jxl.read.biff.BiffException;

public class PostEngagementRequestService extends TestBase {
	static final Logger log = LogManager.getLogger(PostEngagementRequestService.class);
	static ExtentTest logger;
	ReadExcelData readExcelData = new ReadExcelData();
	BypassSslCertificateForPost bypassSslCertificate = new BypassSslCertificateForPost();
	DeleteAndImportDataInDb deleteAndImportDataInDb = new DeleteAndImportDataInDb();
	
	/**
	 * @author shalini
	 * @param csvName
	 */
	@Test(dataProvider="deleteEntryFromDb", priority=0)
	public void deleteAndImportOnDbTest(String csvName) {
		logger = super.extent.createTest(new Object() {
		}.getClass().getEnclosingMethod().getName() + " :: " + csvName);
		deleteAndImportDataInDb.deleteAndImportDataFromDb(csvName);
		logger.pass("Test has been passed for collection name : " + csvName);
	}
	
	@DataProvider(name="deleteEntryFromDb")
	public Object[][] getDataFromCsv(){
		return new Object[][] {
            { "campaign"},
            { "appInformation"},
            { "partner"}
        };
		}

	/**
	 * 
	 * @param url
	 * @param urlParameters
	 */
	@Test(dataProvider = "getEngagementRequestInput", priority=1, enabled=true)
	public void postEngagementRequestService(String url, String urlParameters, String expectStatusCode) {
		System.out.println("status code is : " + expectStatusCode);
		System.out.println("url is : " + url);
		logger = super.extent.createTest(new Object() {
		}.getClass().getEnclosingMethod().getName());
		// Send post request
		int statusCode = bypassSslCertificate.skipSslCertificateForPost(prop.getProperty("base_url")+url, urlParameters, logger);
		int expectedStatusCodeInt = Integer.parseInt(expectStatusCode); // convert string status code to int
		// verify the status code 
		Assert.assertTrue(statusCode == expectedStatusCodeInt,
				"Actual and expected status code are different; actual status code is : " + statusCode
						+ " and expected status code is : " + expectedStatusCodeInt);
		logger.info("actual status code : "+ statusCode + " expected status code : " + expectedStatusCodeInt);
	}

	@SuppressWarnings("static-access")
	@DataProvider(name = "getEngagementRequestInput")
	public Object[][] getDataFromExcel() throws BiffException {
		Object[][] arrayObject = readExcelData.getExcelData(System.getProperty("user.dir") + "/excel/demo.xls",
				"EngagementService");
		return arrayObject;
	}

	@AfterClass
	public void sendReport()
	{
		super.sendReport();
	}
}
